# Remote buitl in Led ESP8266-via-wifi

Control LED via wifi network using Arduino IDE

*  Download arduino IDE and install
    Link download: https://www.arduino.cc/en/main/software
*  Run Arduino
*  Update board for ESP8266
    *  Select menu File->Preferences
    *  Update link on Additional Boards Manager URLs: http://arduino.esp8266.com/stable/package_esp8266com_index.json
    *  Click OK
*  Install board for ESP8266
    *  Select menu Tools->Board: XXXX->Board Manager
    *  Filter to ESP8255
    *  Install
*  Change default board to ESP8266
    *  Select menu Tools->Board: XXXX->NodeMCU 1.0 (ESP12-E Module)
    *  Set Upload Speed to 115200
    *  Set Port COM
*  Lets coding
