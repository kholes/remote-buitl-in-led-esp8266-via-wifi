// Gunakan module WiFi dari board ESP8266
#include <ESP8266WiFi.h>

// Konfigurasi jaringan WiFi
const char* ssid = "Nokia 2";
const char* password = "adamjavas";

// Inisialisasi pin I/O
const int p1 = LED_BUILTIN;

// Gunakan port deault 80
WiFiServer serverESP(80);

void setup() {
  Serial.begin(115200);
  delay(10);
  pinMode(p1, OUTPUT);
  digitalWrite(p1, LOW);

  // Koneksi ke jaringan WiFi
  Serial.print("Connecting to ");
  Serial.print(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("");
  Serial.print("WiFiConnected.");

  serverESP.begin();
  Serial.println("Server started.");

  // Menampilkan informasi IP server ke layar
  Serial.print("Gunakan IP ini pada browser, untuk akses server");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print("/"); 
}

void loop() {
  // put your main code here, to run repeatedly:
  WiFiClient client = serverESP.available();
  if(!client) {
    return;
  }
  Serial.println("New Client");
  while(!client .available()) {
    delay(1);
  }

  String requestESP = client .readStringUntil('\r');
  Serial.println(requestESP);
  client . flush();

  // Buat header HTML
  client .println("HTTP/1.1 200 OK");
  client .println("Content-Type: text/html");
  client .println("");
  client .println("<!DOCTYPE HTML>");
  client .println("<html>");

  // Buat body HTML
  client .println("<h1>PROTOTYPE UNTUK TOILET Q</h1>");
  client .println("<h2>INI BARU CONTOH, SELANJUTNYA IRSYAD YANG AKAN MELANJUTKAN..HE..HE</h2>");
  client .println("Click <a href=\"/LED=nyala\">LED ON</a>");
  client .println("Click <a href=\"/LED=mati\">LED OFF</a>");
  client .println("</br>");

  // Tutup HTML
  client .println("</html>");

  // Membaca input melalui anchor html
  if(requestESP.indexOf("/LED=nyala") != -1) {
    digitalWrite(p1, LOW);
    client .println("LED MENYALA");
  }

  if(requestESP.indexOf("/LED=mati") != -1) {
    digitalWrite(p1, HIGH);
    client .println("LED MATI");
  }

  delay(1);
  Serial.println("Client disconected");
  Serial.println("");
}